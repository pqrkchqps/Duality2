#include "TerrainManager.h"
#include "Duality.h"
#include "Job.h"
#include "TerrainGeneratorWorker.h"
#include <chrono>
#include "NumOfVerticesObject.h"

using namespace std::chrono;


ATerrainManager::ATerrainManager()
{
	PrimaryActorTick.bCanEverTick = true;
}

ATerrainManager::~ATerrainManager()
{

}

void ATerrainManager::BeginPlay()
{
	Super::BeginPlay();

	FString threadName = "TerrainWorkerThread";

	for (int i = 0; i < TerrainConfig.NumberOfThreads; i++)
	{
		GeometryJobs.Emplace();

		WorkerThreads.Add(FRunnableThread::Create
		(new FTerrainGeneratorWorker(this, &TerrainConfig, &GeometryJobs[i]),
			*threadName,
			0, EThreadPriority::TPri_BelowNormal, FPlatformAffinity::GetNoAffinityMask()));
	}
}

void ATerrainManager::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	TimeSinceLastSweep += DeltaSeconds;

	if (isSetup)
	{
		// First check for the tracking actor crossing a tile boundary
		Point oldPos = currentPlayerZone;

		if (TrackingActor->IsValidLowLevel())
		{
			currentPlayerZone.X = floor(TrackingActor->GetActorLocation().X / (TerrainConfig.UnitSize * TerrainConfig.TileXUnits));
			currentPlayerZone.Y = floor(TrackingActor->GetActorLocation().Y / (TerrainConfig.UnitSize * TerrainConfig.TileYUnits));
		}

		Point newPos = currentPlayerZone;

		if (oldPos != newPos)
		{
			Point delta = newPos - oldPos;
			HandleTileFlip(delta);
		}

		// Check for pending jobs
		for (int i = 0; i < TerrainConfig.NumberOfThreads; i++)
		{
			FJob pendingJob;
			if (PendingJobs.Peek(pendingJob))
			{
				// If there's free data to allocate, dequeue and send to worker thread
				if (FreeMeshData[pendingJob.LOD].Num() > 0)
				{
					PendingJobs.Dequeue(pendingJob);
					GetFreeMeshData(pendingJob);
					GeometryJobs[i].Enqueue(pendingJob);
				}
			}
		}

		// Now check for Update jobs
		for (uint8 i = 0; i < TerrainConfig.MeshUpdatesPerFrame; i++)
		{
			FJob updateJob;
			if (UpdateJobs.Dequeue(updateJob))
			{
				milliseconds startMs = duration_cast<milliseconds>(
					system_clock::now().time_since_epoch()
					);

				updateJob.Tile->UpdateMesh(updateJob.LOD, updateJob.IsInPlaceUpdate, updateJob.Vertices, updateJob.Triangles, updateJob.Normals, updateJob.UV0, updateJob.VertexColors, updateJob.Tangents);

				OnTileMeshUpdated.Broadcast(updateJob.Tile);

				int32 updateMS = (duration_cast<milliseconds>(
					system_clock::now().time_since_epoch()
					) - startMs).count();

#ifdef UE_BUILD_DEBUG
				if (updateJob.LOD == 0)
				{
					GEngine->AddOnScreenDebugMessage(0, 5.f, FColor::Red, TEXT("Heightmap gen " + FString::FromInt(updateJob.HeightmapGenerationDuration) + "ms"));
					GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, TEXT("Erosion gen " + FString::FromInt(updateJob.ErosionGenerationDuration) + "ms"));
					GEngine->AddOnScreenDebugMessage(2, 5.f, FColor::Red, TEXT("MeshUpdate " + FString::FromInt(updateMS) + "ms"));
				}
#endif
				ReleaseMeshData(updateJob.LOD, updateJob.Data);
				QueuedTiles.Remove(updateJob.Tile);
			}
		}


		// Now check for LOD sweeps;
		if (TimeSinceLastSweep > SweepInterval)
		{
			SweepLODs();
			TimeSinceLastSweep = 0.0f;
		}
	}
	else
	{
		if (TerrainConfig.NoiseGenerator != nullptr)
		{
			DestroyTiles();
			SpawnTiles(TrackingActor, TerrainConfig, XTiles, YTiles);
		}
	}

	if (isFirstDraw && PendingJobs.IsEmpty() && UpdateJobs.IsEmpty())
	{
		OnInitialTileDrawComplete();
		isFirstDraw = false;
	}
}

void ATerrainManager::DestroyTiles() {
	for (ATile* tile : Tiles) {
		tile->Destroy();
	}
	Tiles.Empty();
}

void ATerrainManager::SweepLODs()
{
	for (ATile* tile : Tiles)
	{
		uint8 newLOD = GetLODForTile(tile);
		// LOD has decreased, we need to generate geometry
		if (tile->GetCurrentLOD() > newLOD && !QueuedTiles.Contains(tile))
		{
			FJob newJob;
			newJob.Tile = tile;
			newJob.LOD = newLOD;
			newJob.IsInPlaceUpdate = true;
			CreateTileRefreshJob(newJob);
		}
	}
}

void ATerrainManager::BeginDestroy()
{
	for (auto& thread : WorkerThreads)
	{
		if (thread != nullptr)
		{
			thread->Kill();
		}
	}

	Super::BeginDestroy();
}

uint8 ATerrainManager::GetLODForTile(ATile* aTile)
{
	FVector diff;

	FVector centreOfTile = aTile->GetCentrePos();

	diff = TrackingActor->GetActorLocation() - centreOfTile;

	float distance = diff.Size();
	uint8 lodIndex = 0;

	for (FLODConfig& lod : TerrainConfig.LODs)
	{
		if (distance < lod.Range)
		{
			return lodIndex;
		}
		++lodIndex;
	}

	return 10;
}

void ATerrainManager::CreateTileRefreshJob(FJob aJob)
{
	if (aJob.LOD != 10)
	{
		PendingJobs.Enqueue(aJob);
		QueuedTiles.Add(aJob.Tile);
	}
}


void ATerrainManager::HandleTileFlip(Point deltaTile)
{
	// this section is still horrible, oh well!
	int32 minX = 999999999;
	int32 maxX = -99999999;
	int32 minY = 999999999;
	int32 maxY = -99999999;

	for (ATile* tile : Tiles)
	{
		if (tile->Offset.X < minX) {
			minX = tile->Offset.X;
		}
		if (tile->Offset.X > maxX) {
			maxX = tile->Offset.X;
		}
		if (tile->Offset.Y < minY) {
			minY = tile->Offset.Y;
		}
		if (tile->Offset.Y > maxY) {
			maxY = tile->Offset.Y;
		}
	}

	for (ATile* tile : Tiles)
	{
		if (deltaTile.X < -0.1 && tile->Offset.X == maxX) {
			tile->Offset.X = minX - 1;
			tile->RepositionAndHide(GetLODForTile(tile));
			FJob job; job.Tile = tile; job.LOD = GetLODForTile(tile); job.IsInPlaceUpdate = false;
			CreateTileRefreshJob(job);
		}
		else if (deltaTile.X > 0.1 && tile->Offset.X == minX) {
			tile->Offset.X = maxX + 1;
			tile->RepositionAndHide(GetLODForTile(tile));
			FJob job; job.Tile = tile; job.LOD = GetLODForTile(tile); job.IsInPlaceUpdate = false;
			CreateTileRefreshJob(job);
		}
		if (deltaTile.Y < -0.1 && tile->Offset.Y == maxY) {
			tile->Offset.Y = minY - 1;
			tile->RepositionAndHide(GetLODForTile(tile));
			FJob job; job.Tile = tile; job.LOD = GetLODForTile(tile); job.IsInPlaceUpdate = false;
			CreateTileRefreshJob(job);
		}
		else if (deltaTile.Y > 0.1 && tile->Offset.Y == minY) {
			tile->Offset.Y = maxY + 1;
			tile->RepositionAndHide(GetLODForTile(tile));
			FJob job; job.Tile = tile; job.LOD = GetLODForTile(tile); job.IsInPlaceUpdate = false;
			CreateTileRefreshJob(job);
		}
	}
}

bool ATerrainManager::GetFreeMeshData(FJob& aJob)
{
	// No free mesh data
	if (FreeMeshData[aJob.LOD].Num() < 1)
	{
		return false;
	}
	else
	{
		FMeshData* dataToUse;
		// Use the first free data set, there'll always be one, we checked!
		for (FMeshData* data : FreeMeshData[aJob.LOD])
		{
			dataToUse = data;
			break;
		}
		// Add to the in use set
		InUseMeshData[aJob.LOD].Add(dataToUse);
		// Remove from the Free set
		FreeMeshData[aJob.LOD].Remove(dataToUse);

		aJob.Vertices = &dataToUse->Vertices;
		aJob.Triangles = &dataToUse->Triangles;
		aJob.Normals = &dataToUse->Normals;
		aJob.UV0 = &dataToUse->UV0;
		aJob.VertexColors = &dataToUse->VertexColors;
		aJob.Tangents = &dataToUse->Tangents;
		aJob.HeightMap = &dataToUse->HeightMap;
		aJob.Data = dataToUse;
		return true;
	}

	return false;
}

void ATerrainManager::ReleaseMeshData(uint8 aLOD, FMeshData* aDataToRelease)
{
	InUseMeshData[aLOD].Remove(aDataToRelease);
	FreeMeshData[aLOD].Add(aDataToRelease);
}

/** Allocates data structures and pointers for mesh data **/
void ATerrainManager::AllocateAllMeshDataStructures()
{
	for (uint8 lod = 0; lod < TerrainConfig.LODs.Num(); ++lod)
	{
		MeshData.Add(FLODMeshData());
		FreeMeshData.Add(TSet<FMeshData*>());
		InUseMeshData.Add(TSet<FMeshData*>());

		MeshData[lod].Data.Reserve(TerrainConfig.MeshDataPoolSize);

		for (int j = 0; j < TerrainConfig.MeshDataPoolSize; ++j)
		{
			MeshData[lod].Data.Add(FMeshData());
			AllocateDataStructuresForLOD(&MeshData[lod].Data[j], &TerrainConfig, lod);
		}
	}

	for (uint8 lod = 0; lod < TerrainConfig.LODs.Num(); ++lod)
	{
		for (int j = 0; j < TerrainConfig.MeshDataPoolSize; ++j)
		{
			FreeMeshData[lod].Add(&MeshData[lod].Data[j]);
		}
	}
}

/************************************************************************/
/*  Main setup method, pass in the config struct and other details
/*				Spawns the Tile actors into place and sets them up
/************************************************************************/
void ATerrainManager::SpawnTiles(AActor* aTrackingActor, const FTerrainConfig aTerrainConfig, const int32 aXTiles, const int32 aYTiles)
{
	TerrainConfig = aTerrainConfig;
	TrackingActor = aTrackingActor;
	XTiles = aXTiles;
	YTiles = aYTiles;
	currentPlayerZone.X = 0; currentPlayerZone.Y = 0;

	AllocateAllMeshDataStructures();

	WorldOffset = FVector((XTiles / 2.0f) * TerrainConfig.TileXUnits * TerrainConfig.UnitSize, (YTiles / 2.0f) * TerrainConfig.TileYUnits * TerrainConfig.UnitSize, 0.0f);

	if (aTrackingActor)
	{
		currentPlayerZone.X = floor(aTrackingActor->GetActorLocation().X / ((TerrainConfig.UnitSize * TerrainConfig.TileXUnits) - WorldOffset.X));
		currentPlayerZone.Y = floor(aTrackingActor->GetActorLocation().Y / ((TerrainConfig.UnitSize * TerrainConfig.TileYUnits) - WorldOffset.Y));
	}

	for (int32 i = 0; i < XTiles * YTiles; ++i)
	{
		Tiles.Add(GetWorld()->SpawnActor<ATile>(ATile::StaticClass(),
			FVector((TerrainConfig.TileXUnits * TerrainConfig.UnitSize * GetXYfromIdx(i).X) - WorldOffset.X,
			(TerrainConfig.TileYUnits * TerrainConfig.UnitSize * GetXYfromIdx(i).Y) - WorldOffset.Y, 0.0f) - WorldOffset, FRotator(0.0f)));

	}

	int32 tileIndex = 0;
	for (ATile* tile : Tiles)
	{
		tile->SetupTile(GetXYfromIdx(tileIndex), &TerrainConfig, WorldOffset);
		tile->RepositionAndHide(GetLODForTile(tile));
		FJob job; job.Tile = tile; job.LOD = GetLODForTile(tile); job.IsInPlaceUpdate = false;
		CreateTileRefreshJob(job);
		++tileIndex;
	}

	isSetup = true;
}

/************************************************************************/
/*  Allocates all the data structures for a single LOD mesh data
/*		Includes setting up triangles etc.
/************************************************************************/
bool ATerrainManager::AllocateDataStructuresForLOD(FMeshData* aData, FTerrainConfig* aConfig, const uint8 aLOD)
{
	NumOfVerticesObject numOfVerticesObject = NumOfVerticesObject(&TerrainConfig, aConfig, aLOD);
	int32 numTotalVertices = numOfVerticesObject.numTotalVertices;
	aData->InitializeMeshData(numOfVerticesObject, aLOD, &TerrainConfig, aConfig);

	return true;
}