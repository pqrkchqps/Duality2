// @2017 Dreamer Assist Corporation ALL RIGHTS RESERVED

#include "EnergyMatrix.h"
#include "Kismet/GameplayStatics.h"
#include "EnergyBall.h"
#include "Engine/World.h"
#include "EColor.h"

// Sets default values
AEnergyMatrix::AEnergyMatrix()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	DummyRoot = CreateDefaultSubobject <USceneComponent>(FName("Dummy Root"));
	SetRootComponent(DummyRoot);

	PieceRotator = CreateDefaultSubobject<USceneComponent>(FName("Piece Rotator"));
	PieceRotator->AttachTo(GetRootComponent());

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(FName("Spring Arm"));
	SpringArm->AttachTo(GetRootComponent());

	SpringArm->bDoCollisionTest = false;
	SpringArm->bInheritPitch = false;
	SpringArm->bInheritRoll = false;
	SpringArm->bInheritYaw = false;

	//Update camera's arm length for the size
	float zoomStart = CAMERA_ZOOM_START;

	SpringArm->TargetArmLength = zoomStart + CAMERA_ZOOM_STEP;

	Camera = CreateDefaultSubobject<UCameraComponent>(FName("Camera"));
	Camera->AttachTo(SpringArm);

}

// Called when the game starts or when spawned
void AEnergyMatrix::BeginPlay()
{
	Super::BeginPlay();
	this->BuildWholeEnergyMatrix(6);
}

// Called every frame
void AEnergyMatrix::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEnergyMatrix::SetupCamera(int32 n) {
	UWorld * gWorld = GetWorld();

	//Update camera's arm length for the new size
	float zoomStart = CAMERA_ZOOM_START;

	SpringArm->TargetArmLength = zoomStart + (n)*CAMERA_ZOOM_STEP;

	//Set PieceRotator and camera's arm to the center of the new cube
	PieceRotator->SetRelativeLocation(FVector(0.0f, ((n-1)*CUBE_EXTENT) / 2, ((n-1)*CUBE_EXTENT) / 2));
	SpringArm->SetRelativeLocation(FVector(0.0f, ((n-1)*CUBE_EXTENT) / 2, ((n-1)*CUBE_EXTENT) / 2));
	SpringArm->SetRelativeRotation(FRotator::MakeFromEuler(FVector(0, 0, 0)));
}

void AEnergyMatrix::CreatePlaneOfEnergyBalls(int32 n) {

}

void AEnergyMatrix::BuildWholeEnergyMatrix(int32 n)
{
	UWorld * gWorld = GetWorld();
	SetupCamera(n);
	//Create cube based on its size
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			//Spawn only pieces that belongs to a wall
			//Spawn a rubiks piece
			FActorSpawnParameters params;
			params.Owner = this;
			AEnergyBall * ball = gWorld->SpawnActor<AEnergyBall>(PieceClass, FVector(0.0f, CUBE_EXTENT * i, CUBE_EXTENT * j), FRotator(0.0f, 0.0f, 0.0f), params);
			if (i == 0 && j == 0) {
				ball->SetColor(EColor::Color::RedSelected);
			}
			else {
				ball->SetColor(EColor::Color::Red);
			}
			ball->AttachRootComponentToActor(this, NAME_None, EAttachLocation::KeepWorldPosition, false);
			ball->Tags.Add(CUBE_PIECE_TAG);
			//Balls.Add(piece);
		}
	}
}

