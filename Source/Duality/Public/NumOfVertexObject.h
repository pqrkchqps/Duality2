#pragma once
#include "Duality.h"
struct NumOfVerticesObject
{
	int32 numXVertices, numYVertices, numTotalVertices;
	Point(int32 aX, int32 aY)
	{
		X = aX;
		Y = aY;
	}

	bool operator==(const Point& Src) const
	{
		return (X == Src.X) && (Y == Src.Y);
	}

	Point operator-(const Point& Src) const
	{
		return Point(X - Src.X, Y - Src.Y);
	}

	bool operator!=(const Point& Src) const
	{
		return (X != Src.X) || (Y != Src.Y);
	}

	friend FORCEINLINE uint32 GetTypeHash(const Point& point)
	{
		return FCrc::MemCrc_DEPRECATED(&point, sizeof(Point));
	}

};