#pragma once

#include "Duality.h"
#include "Point.h"
#include "RuntimeMeshComponent.h"
#include "TerrainConfig.h"
#include "Tile.generated.h"

class ATerrainManager;
//struct FTerrainConfig;

enum ELODStatus { NOT_CREATED, CREATED, TRANSITION };

UCLASS()
class ATile : public AActor
{
	GENERATED_BODY()

	TMap<uint8, URuntimeMeshComponent*> MeshComponents;
	TMap<uint8, UMaterialInstanceDynamic*> MaterialInstances;
	UMaterialInstance* MaterialInstance;
	UMaterial* Material;
	TMap<uint8, ELODStatus> LODStatus;

	float LODTransitionOpacity = 0.0f;

	uint8 CurrentLOD;
	uint8 PreviousLOD;

	USphereComponent* SphereComponent;

public:
	ATile();
	~ATile();

public:

	Point Offset;
	FVector WorldOffset;
	FTerrainConfig* TerrainConfigMaster;

	uint8 GetCurrentLOD();

	void RepositionAndHide(uint8 aNewLOD);

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	void SetupTile(Point aOffset, FTerrainConfig* aTerrainConfig, FVector aWorldOffset);
	void UpdateMesh(uint8 aLOD, bool aIsInPlaceUpdate, TArray<FVector>*	aVertices,
		TArray<int32>*		aTriangles,
		TArray<FVector>*	aNormals,
		TArray<FVector2D>*	aUV0,
		TArray<FColor>*		aVertexColors,
		TArray<FRuntimeMeshTangent>* aTangents);

	FVector GetCentrePos();

	UMaterialInstanceDynamic* GetMaterialInstanceDynamic(const uint8 aLOD);

};