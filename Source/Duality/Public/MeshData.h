#pragma once
#include "Duality.h"
#include "RuntimeMeshComponent.h"
#include "TerrainConfig.h"
#include "NumOfVerticesObject.h"
#include "Math/Vector.h"
#include "Meshdata.generated.h"

/** Defines the data required for a single procedural mesh section */
USTRUCT()
struct FMeshData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh Data Struct")
		TArray<FVector> Vertices;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh Data Struct")
		TArray<int32> Triangles;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh Data Struct")
		TArray<FVector> Normals;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh Data Struct")
		TArray<FVector2D> UV0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh Data Struct")
		TArray<FColor> VertexColors;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh Data Struct")
		TArray<FRuntimeMeshTangent> Tangents;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh Data Struct")
		TArray<FVector> HeightMap;

	void InitializeMeshData(NumOfVerticesObject numOfVerts, const uint8 aLOD, FTerrainConfig* TerrainConfig, FTerrainConfig* aConfig) {
		int32 numTotalVertices = numOfVerts.numTotalVertices;
		Vertices.Reserve(numTotalVertices);
		Normals.Reserve(numTotalVertices);
		UV0.Reserve(numTotalVertices);
		VertexColors.Reserve(numTotalVertices);
		Tangents.Reserve(numTotalVertices);
		HeightMap.Reserve((numOfVerts.numXVertices + 2) * (numOfVerts.numYVertices + 2));

		// Generate the per vertex data sets
		for (int32 i = 0; i < (numTotalVertices); ++i)
		{
			Vertices.Emplace(0.0f);
			Normals.Emplace(0.0f, 0.0f, 1.0f);
			UV0.Emplace(0.0f, 0.0f);
			VertexColors.Emplace(FColor::Black);
			Tangents.Emplace(0.0f, 0.0f, 0.0f);
		}

		HeightMap.Reserve((numOfVerts.numXVertices + 2) * (numOfVerts.numYVertices + 2));
		for (int32 i = 0; i < (numOfVerts.numXVertices + 2) * (numOfVerts.numYVertices + 2); ++i)
		{
			HeightMap.Emplace(0.0f);
		}

		// Triangle indexes
		int32 terrainTris = ((numOfVerts.numXVertices - 1) * (numOfVerts.numYVertices - 1) * 6);
		int32 skirtTris = (((numOfVerts.numXVertices - 1) * 2) + ((numOfVerts.numYVertices - 1) * 2)) * 6;
		int32 numTris = terrainTris + skirtTris;
		Triangles.Reserve(numTris);
		for (int32 i = 0; i < numTris; ++i)
		{
			Triangles.Add(i);
		}

		// Now calculate triangles and UVs
		int32 triCounter = 0;
		int32 thisX, thisY;
		int32 rowLength;

		rowLength = aLOD == 0 ? aConfig->TileXUnits + 1 : (aConfig->TileXUnits / TerrainConfig->LODs[aLOD].ResolutionDivisor + 1);
		float maxUV = aLOD == 0 ? 1.0f : 1.0f / aLOD;

		int32 exX = aLOD == 0 ? aConfig->TileXUnits : (aConfig->TileXUnits / TerrainConfig->LODs[aLOD].ResolutionDivisor);
		int32 exY = aLOD == 0 ? aConfig->TileYUnits : (aConfig->TileYUnits / TerrainConfig->LODs[aLOD].ResolutionDivisor);

		for (int32 y = 0; y < exY; ++y)
		{
			for (int32 x = 0; x < exX; ++x)
			{

				thisX = x;
				thisY = y;
				//TR
				Triangles[triCounter] = thisX + ((thisY + 1) * (rowLength));
				triCounter++;
				//BL
				Triangles[triCounter] = (thisX + 1) + (thisY * (rowLength));
				triCounter++;
				//BR
				Triangles[triCounter] = thisX + (thisY * (rowLength));
				triCounter++;

				//BL
				Triangles[triCounter] = (thisX + 1) + (thisY * (rowLength));
				triCounter++;
				//TR
				Triangles[triCounter] = thisX + ((thisY + 1) * (rowLength));
				triCounter++;
				// TL
				Triangles[triCounter] = (thisX + 1) + ((thisY + 1) * (rowLength));
				triCounter++;

				//TR
				UV0[thisX + ((thisY + 1) * (rowLength))] = FVector2D(thisX * maxUV, (thisY + 1.0f) * maxUV);
				//BR
				UV0[thisX + (thisY * (rowLength))] = FVector2D(thisX * maxUV, thisY * maxUV);
				//BL
				UV0[(thisX + 1) + (thisY * (rowLength))] = FVector2D((thisX + 1.0f) * maxUV, thisY * maxUV);
				//TL
				UV0[(thisX + 1) + ((thisY + 1) * (rowLength))] = FVector2D((thisX + 1.0f)* maxUV, (thisY + 1.0f) * maxUV);
			}
		}
	}
};