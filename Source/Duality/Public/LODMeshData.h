#pragma once
#include "MeshData.h"
#include "LODMeshData.generated.h"

USTRUCT()
struct FLODMeshData
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FMeshData> Data;

};