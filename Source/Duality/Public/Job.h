#pragma once
#include "RuntimeMeshComponent.h"
#include "Tile.h"
#include "Job.generated.h"

struct FMeshData;

USTRUCT()
struct FJob
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ATile* Tile;

	FMeshData* Data;

	TArray<FVector>*	Vertices;
	TArray<int32>*		Triangles;
	TArray<FVector>*	Normals;
	TArray<FVector2D>*	UV0;
	TArray<FColor>*	VertexColors;
	TArray<FRuntimeMeshTangent>* Tangents;
	TArray<FVector>* HeightMap;
	TArray<float>* DespositionMap;

	int32 HeightmapGenerationDuration;
	int32 ErosionGenerationDuration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CashGen")
	uint8 LOD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CashGen")
	bool IsInPlaceUpdate;
};