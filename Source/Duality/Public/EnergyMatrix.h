// @2017 Dreamer Assist Corporation ALL RIGHTS RESERVED

#pragma once
#include "Duality.h"
#include "GameFramework/Actor.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "EnergyBall.h"
#include "EnergyMatrix.generated.h"

#define CAMERA_ZOOM_START 500
#define CAMERA_ZOOM_STEP 288
#define CUBE_EXTENT 288
#define CUBE_PIECE_TAG "EnergyBall" 


UCLASS()
class DUALITY_API AEnergyMatrix : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnergyMatrix();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(Category = Duality, EditAnywhere)
	TSubclassOf<AEnergyBall> PieceClass;

	UPROPERTY(Category = Duality, VisibleAnywhere)
	USceneComponent * DummyRoot;

	UPROPERTY(Category = Duality, VisibleAnywhere)
	USpringArmComponent * SpringArm;

	UPROPERTY(Category = Duality, VisibleAnywhere)
	USceneComponent * PieceRotator;

	UPROPERTY(Category = Duality, VisibleAnywhere)
	UCameraComponent * Camera;
private:
	void CreatePlaneOfEnergyBalls(int32 n);
	void BuildWholeEnergyMatrix(int32 n);
	void SetupCamera(int32 n);
};
