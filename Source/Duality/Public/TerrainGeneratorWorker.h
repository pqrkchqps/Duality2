#pragma once

#include "Duality.h"
#include "TerrainManager.h"
#include "TerrainConfig.h"

struct FJob;

class FTerrainGeneratorWorker : public FRunnable
{
	ATerrainManager* pTerrainManager;
	FTerrainConfig* pTerrainConfig;
	TQueue<FJob, EQueueMode::Spsc>* inputQueue;
	FJob workJob;
	uint8 workLOD;

	TArray<FVector>*	pVertices;
	TArray<int32>*		pTriangles;
	TArray<FVector>*	pNormals;
	TArray<FVector2D>*	pUV0;
	TArray<FColor>*		pVertexColors;
	TArray<FRuntimeMeshTangent>* pTangents;
	TArray<FVector>* pHeightMap;
	TArray<float>* pDepositionMap;

	bool IsThreadFinished;

	void prepMaps();
	void ProcessTerrainMap();
	void AddDepositionToHeightMap();
	void ProcessSingleDropletErosion();
	void ProcessPerBlockGeometry();
	void ProcessPerVertexTasks();
	void ProcessSkirtGeometry();

	void erodeHeightMapAtIndex(int32 aX, int32 aY, float aAmount);
	FVector GetNormalFromHeightMapForVertex(const int32 vertexX, const int32 vertexY);
	FRuntimeMeshTangent GetTangentFromNormal(const FVector aNormal);
	void UpdateOneBlockGeometry(const int aX, const int aY, int32& aVertCounter, int32& triCounter);

	int32 GetNumberOfNoiseSamplePoints();

public:

	FTerrainGeneratorWorker(ATerrainManager* aTerrainManager,
		FTerrainConfig* aTerrainConfig,
		TQueue<FJob, EQueueMode::Spsc>* anInputQueue);

	virtual ~FTerrainGeneratorWorker();

	virtual bool Init();
	virtual uint32 Run();
	virtual void Stop();
	virtual void Exit();

};


