#pragma once
#include "LODConfig.generated.h"

USTRUCT()
struct FLODConfig
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Range;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 ResolutionDivisor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isCollisionEnabled;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isTesselationEnabled;

};