#pragma once
#include "Duality.h"
#include "TerrainConfig.h"

struct NumOfVerticesObject
{
	int32 numXVertices, numYVertices, numTotalVertices;
	NumOfVerticesObject(FTerrainConfig* TerrainConfig, FTerrainConfig* aConfig, const uint8 aLOD)
	{
		numXVertices = aLOD == 0 ? aConfig->TileXUnits + 1 : (aConfig->TileXUnits / TerrainConfig->LODs[aLOD].ResolutionDivisor) + 1;
		numYVertices = aLOD == 0 ? aConfig->TileYUnits + 1 : (aConfig->TileYUnits / TerrainConfig->LODs[aLOD].ResolutionDivisor) + 1;
		numTotalVertices = numXVertices * numYVertices + (aConfig->TileXUnits * 2) + (aConfig->TileYUnits * 2) + 4;
	}
};