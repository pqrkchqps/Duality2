#pragma once

UENUM()
namespace EColor {
	enum Color { Blue, Green, Red, Yellow, White, Violet, BlueSelected, GreenSelected, RedSelected, YellowSelected, WhiteSelected, VioletSelected };
}