#pragma once
#include "GameFramework/Actor.h"
#include "Job.h"
#include "TerrainConfig.h"
#include "MeshData.h"
#include "Point.h"
#include "LODMeshData.h"
#include "Tile.h"
#include "TerrainManager.generated.h"


UCLASS(BlueprintType, Blueprintable)
class ATerrainManager : public AActor
{
	GENERATED_BODY()

	bool isSetup = false;
	Point currentPlayerZone = Point(0, 0);
	TArray<FRunnableThread*> WorkerThreads;

	void HandleTileFlip(Point deltaTile);
	UPROPERTY()
	TArray<FLODMeshData> MeshData;
	TArray<TSet<FMeshData*>> FreeMeshData;
	TArray<TSet<FMeshData*>> InUseMeshData;
	FVector WorldOffset;
	TArray<ATile*> Tiles;

	bool GetFreeMeshData(FJob& aJob);
	void ReleaseMeshData(uint8 aLOD, FMeshData* aDataToRelease);
	void AllocateAllMeshDataStructures();
	bool AllocateDataStructuresForLOD(FMeshData* aData, FTerrainConfig* aConfig, const uint8 aLOD);
	void CreateTileRefreshJob(FJob aJob);
	void SweepLODs();
	uint8 GetLODForTile(ATile* aTile);

	Point GetXYfromIdx(const int32 idx) { return Point(idx % XTiles, idx / YTiles); }
	float TimeSinceLastSweep;
	const float SweepInterval = 0.1f;

	bool isFirstDraw = true;

public:
	ATerrainManager();
	~ATerrainManager();


	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTileMeshUpdated, ATile*, Tile);
	UPROPERTY(BlueprintAssignable)
		FTileMeshUpdated OnTileMeshUpdated;

	UFUNCTION(BlueprintImplementableEvent, Category = "World")
		void OnInitialTileDrawComplete();

	UPROPERTY()
		AActor* TrackingActor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CashGen")
		int32 XTiles;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CashGen")
		int32 YTiles;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CashGen")
		FTerrainConfig TerrainConfig;

	/* Spawn the terrain */
	UFUNCTION(BlueprintCallable, Category = "CashGen")
	void SetUpTerrain(UUFNNoiseGenerator* aNoiseGen, UUFNNoiseGenerator* aBiomeBlendGen, AActor* aTrackingActor) {
		TerrainConfig.NoiseGenerator = aNoiseGen;
		TerrainConfig.BiomeBlendGenerator = aBiomeBlendGen;
		TrackingActor = aTrackingActor;
		WaitForJobsThenResetTerrain();
	}

	void WaitForJobsThenResetTerrain() {
		FTimerHandle UnusedHandle;
		if (PendingJobs.IsEmpty() && UpdateJobs.IsEmpty()) {
			isFirstDraw = true;
			isSetup = false;
		}
		else {
			GetWorldTimerManager().SetTimer(UnusedHandle, this, &ATerrainManager::WaitForJobsThenResetTerrain, 0.5f, false);
		}
	}

	TQueue<FJob, EQueueMode::Spsc> PendingJobs;
	TArray<TQueue<FJob, EQueueMode::Spsc>> GeometryJobs;
	TQueue<FJob, EQueueMode::Mpsc> UpdateJobs;

	TSet<ATile*> QueuedTiles;

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	void SpawnTiles(AActor* aTrackingActor, const FTerrainConfig aTerrainConfig, const int32 aXTiles, const int32 aYTiles);
	void DestroyTiles();

	void BeginDestroy() override;
};
